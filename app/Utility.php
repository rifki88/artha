<?php
namespace App;

use DB;

class Utility
{
	public function getUser($id)
    {
    	$fullname = '';
    	$model = DB::table('mst_employee')
                        ->where('id','=',$id)
                        ->first();
    	if($model)
    		$fullname = $model->full_name;

    	return $fullname;
    }

    public function getDetailAddress($id) //country_id
    {
        $data = DB::table('mst_villages as v')
                ->join('mst_districts as d','v.district_id','=','d.id')
                ->join('mst_regencies as r','d.regency_id','=','r.id')
                ->join('mst_provinces as p','r.provinces_id','=','p.id')
                ->join('mst_countries as c','p.country_id','=','c.id')
                ->select('v.name as village', 
                        'd.name as district', 
                        'r.name as regency', 
                        'p.name as province', 
                        'c.name as country',
                        'v.id as vid',
                        'd.id as did',
                        'r.id as rid',
                        'p.id as pid',
                        'c.id as cid'
                )
                ->first();
        $village = $data->village;
        $district = $data->district;
        $regency = $data->regency;
        $province = $data->province;
        $country = $data->country;
        return [
            'village'  => $village,
            'district'  => $district,
            'regency'  => $regency,
            'province'  => $province,
            'country'  => $country,
            'village_id'  => $data->vid,
            'district_id'  => $data->did,
            'regency_id'  => $data->rid,
            'province_id'  => $data->pid,
            'country_id'  => $data->cid
        ];
    }
}
?>