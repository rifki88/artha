<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MstEmployee extends Authenticatable
{
    use Notifiable;

    const ACTIVE    = 1;
    const INACTIVE  = 0;

    public $country;
    public $hidden_birth_date;

    protected $table = "mst_employee"; 

    protected $primaryKey = 'id';

    protected $fillable = ['email', 'username', 'password', 'ip_address', 
    						'full_name', 'gender', 'birth_place', 
    						'birth_date', 'address', 'village_id', 'zip_code', 
    						'phone', 'photo', 'active', 'created_by', 
    						'updated_by', 'deleted_date', 'deleted_by', 'is_deleted',
                            'country','hidden_birth_date'];

    protected $hidden = ['password',  'remember_token'];
}
