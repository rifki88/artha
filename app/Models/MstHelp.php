<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstHelp extends Model
{
    protected $table = 'mst_help';

    protected $fillable = ['title', 'sub_title', 'desc', 'iframe', 'active', 'created_by', 'updated_by', 'deleted_date', 'deleted_by', 'is_deleted'];
}
