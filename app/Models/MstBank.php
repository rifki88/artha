<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstBank extends Model
{
    protected $table = 'mst_bank';

    protected $fillable = ['name','card_type','image_logo','active','created_by','updated_by'];

    public function cardTypeList()
    {
    	return [
    		'C'	=> 'Credit',
    		'D'	=> 'Debet'
    	];
    }

    public function getCardType()
    {
    	$card = '';
    	foreach($this->cardTypeList() as $key => $value)
    	{
    		if($this->card_type == $key)
    		{
    			$card = $value;
    			break;
    		}
    	}
    	return $card;
    }
}
