<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstProvinces extends Model
{
    protected $table = 'mst_provinces';
}
