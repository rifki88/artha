<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstPriceClass extends Model
{
    protected $table = 'mst_price_class';
}
