<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecModule extends Model
{
    protected $table = 'sec_module';

    protected $fillable = ['module','link','icon','sort','parent','active','created_by','updated_by'];
}
