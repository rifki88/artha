<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstDistricts extends Model
{
    protected $table = 'mst_districts';
}
