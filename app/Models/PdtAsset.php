<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PdtAsset extends Model
{
    protected $table = 'pdt_asset';

    protected $fillable = ['category_asset_id','asset_name','desc',
    						'owner_name','owner_ktp_number','owner_kk_number',
    						'price_njop','price_market','credit_tenor',
    						'invesment_tenor','terms_conds_id','active',
    						'created_by','updated_by','deleted_date',
    						'deleted_by','is_deleted','images',
    						'file_resume'];
}
