<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstMember extends Model
{
    protected $table = 'mst_member';

    protected $fillable = ['member','active','created_by','updated_by'];
}
