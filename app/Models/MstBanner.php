<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstBanner extends Model
{
    protected $table = 'mst_banner';

    protected $fillable = ['title','subtitle','desc','image','video','iframe','active','created_by','updated_by','deleted_date','deleted_by','is_deleted'];
}
