<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstCountries extends Model
{
    protected $table = 'mst_countries';
}
