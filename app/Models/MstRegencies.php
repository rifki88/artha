<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstRegencies extends Model
{
     protected $table = 'mst_regencies';
}
