<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstMember;
use Illuminate\Support\Facades\Auth;
use DataTables;
use DB;
use App\Utility;

class MstMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.member.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new MstMember();
        return view('master.member.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'member'  => 'required|string|unique:mst_member,member,1,is_deleted',
            'active'=> 'required'
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;

        $data = [
            'member'    => $request->member,
            'active'    => $active,
            'created_by'=> $userId,
            'updated_by'=> $userId
        ];

        $model = MstMember::create($data);
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = MstMember::findOrFail($id);
        $uti = new Utility();
        return view('master.member.detail', compact(['model','uti']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = MstMember::findOrFail($id);
        return view('master.member.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'member'  => 'required|string|unique:mst_member,member,'.$id.',id,is_deleted,0',
            'active'=> 'required'
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;

        $data = [
            'member'    => $request->member,
            'active'    => $active,
            'created_by'=> $userId,
            'updated_by'=> $userId
        ];

        $model = MstMember::findOrFail($id);
        $model->update($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $model = MstMember::findOrFail($id);
        
        // $data = [
        //     'deleted_date'  => date('Y-m-d H:i:s'),
        //     'deleted_by'    => Auth::user()->sec_employee_id,
        //     'is_deleted'    => '1'
        // ];

        // $model->update($data);
        $deleted_date   = date('Y-m-d H:i:s');
        $deleted_by     = Auth::user()->id;

        DB::update("update mst_member set 
            deleted_date='".$deleted_date."',
            deleted_by = ".$deleted_by.",
            is_deleted = 1
            where id = ".$id."");
    }

    public function dataTable()
    {
        $model = MstMember::query();
        $model->where('is_deleted','<>','1');
        // $model = DB::table('mst_member')
        //                 ->where('is_deleted','<>','1')
        //                 ->get();
        return DataTables::of($model)
            // ->addColumn('checkbox', '<input type="checkbox" id="'.$model->sec_role_id.'" name="checkbox">' )
            ->addColumn('action', function($model){
                return view('master.member.action', [
                    'model' => $model,
                    'url_show'=> route('member.show', $model->id),
                    'url_edit'=> route('member.edit', $model->id),
                    'url_destroy'=> route('member.destroy', $model->id)
                ]);
            })
            ->editColumn('created_at', function($model){
                return date('d-m-Y H:i:s', strtotime($model->created_at));
            })
            ->editColumn('created_by', function($model){
                $uti = new utility();
                return $uti->getUser($model->created_by);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'create_at', 'created_by'])
            ->make(true);
    }
}
