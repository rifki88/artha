<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstBanner;
use App\Models\MstPage;
use App\Models\MstBannerDetail;
use Illuminate\Support\Facades\Auth;
use DataTables;
use DB;
use App\Utility;

class MstBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.banner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new MstBanner();
        return view('master.banner.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'     => 'required|string',
            'subtitle'  => 'required|string',
            'desc'      => 'required|string',
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;

        $image_filename = '';
        $video_filename = '';

        $image = $request->file('image');
        if($image != null)
        {
            $image_filename = time().$image->getClientOriginalName();
            $path = base_path().'/public/images/banner/';
            $image->move($path, $image_filename);
        }

        $video = $request->file('video');
        if($video != null)
        {
            $video_filename = time().$video->getClientOriginalName();
            $path = base_path().'/public/video/banner/';
            $video->move($path, $video_filename);
        }

        $data = [
            'title'     => $request->title,
            'subtitle'  => $request->subtitle,
            'desc'      => $request->desc,
            'image'     => $image_filename,
            'video'     => $video_filename,
            'iframe'    => $request->iframe,
            'active'    => $active,
            'created_by'=> $userId,
            'updated_by'=> $userId
        ];

        $model = MstBanner::create($data);
        if($model)
            return redirect('master/banner/setposition/'.$model->id)->with('success','Success');
        else
            Redirect::back()->withErrors(['error', 'Failed']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = MstBanner::findOrFail($id);

        $detail = DB::table('mst_banner_detail as a')
                ->join('mst_position as b','a.position_id','=','b.id')
                ->join('mst_page as c','b.page_id','=','c.id')
                ->where('a.banner_id',$id)
                ->select('c.name as page','b.name as position','a.*')
                ->get();

        $uti = new Utility;

        return view('master.banner.detail', compact(['model','detail','uti']));
    }

    public function setposition($id)
    {
        $model = MstBanner::findOrFail($id);
        $page = MstPage::where('active','1')
                    ->where('is_deleted','0')
                    ->pluck('name', 'id')
                    ->all();
        $bannerdetail = DB::table('mst_banner_detail as a')
                        ->join('mst_banner as b','a.banner_id','=','b.id')
                        ->join('mst_position as c','a.position_id','=','c.id')
                        ->join('mst_page as d','c.page_id','=','d.id')
                        ->where('a.banner_id',$id)
                        ->select('d.name as page','c.name as position','a.*')
                        ->get();
        return view('master.banner.setposition', compact(['model','page','bannerdetail']));
    }

    public function set(Request $request)
    {
        $this->validate($request, [
            'page'     => 'required',
            'position' => 'required'
        ]);

        $data = [
            'banner_id'     => $request->banner,
            'position_id'   => $request->position,
            'order'         => $request->order
        ];

        $model = MstBannerDetail::create($data);
        return $model;
    }

    public function deleteposition($b, $p)
    {
        DB::table('mst_banner_detail')
            ->where('banner_id',$b)
            ->where('position_id',$p)
            ->delete();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = MstBanner::findOrFail($id);
        return view('master.banner.update', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'     => 'required|string',
            'subtitle'  => 'required|string',
            'desc'      => 'required|string',
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;

        $image_filename = '';
        $video_filename = '';

        $old_image = '';
        $old_video = '';

        $model = MstBanner::findOrFail($id);

        $data = [
            'title'     => $request->title,
            'subtitle'  => $request->subtitle,
            'desc'      => $request->desc,
            'iframe'    => $request->iframe,
            'active'    => $active,
            'updated_by'=> $userId
        ];

        $image = $request->file('image');
        if($image != null)
        {
            $image_filename = time().$image->getClientOriginalName();
            $path = base_path().'/public/images/banner/';
            $image->move($path, $image_filename);

            $data = array_merge($data, ['image'=>$image_filename]);

            if($model->image != null)
                $old_image = $model->image;
        }

        $video = $request->file('video');
        if($video != null)
        {
            $video_filename = time().$video->getClientOriginalName();
            $path = base_path().'/public/video/banner/';
            $video->move($path, $video_filename);

            $data = array_merge($data, ['video'=>$video_filename]);

            if($model->video != null)
                $old_video = $model->video;
        }

        if($model->update($data))
        {
            if($old_image != null)
                unlink(base_path().'/public/images/banner/'.$old_image);

            if($old_video != null)
                unlink(base_path().'/public/video/banner/'.$old_video);

            return redirect('master/banner/setposition/'.$model->id)->with('success','Success');
        }
        else
            Redirect::back()->withErrors(['error', 'Failed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $deleted_date   = date('Y-m-d H:i:s');
        $deleted_by     = Auth::user()->id;

        $delete = DB::update("update mst_banner set 
            deleted_at = '".$deleted_date."',
            deleted_by = ".$deleted_by.",
            is_deleted = 1
            where id = ".$id."");

        if($delete)
            return redirect('master/banner')->with('success', 'Deleted');
        else
            return redirect('master/banner/show/'.$id)->with('error', 'Failed');
    }

    public function destroy($id)
    {
        $deleted_date   = date('Y-m-d H:i:s');
        $deleted_by     = Auth::user()->id;

        DB::update("update mst_banner set 
            deleted_at = '".$deleted_date."',
            deleted_by = ".$deleted_by.",
            is_deleted = 1
            where id = ".$id."");
    }

    public function dataTable()
    {
        $model = MstBanner::query();
        $model->where('is_deleted','<>','1');

        return DataTables::of($model)
            // ->addColumn('checkbox', '<input type="checkbox" id="'.$model->sec_role_id.'" name="checkbox">' )
            ->addColumn('action', function($model){
                return view('master.banner.action', [
                    'model' => $model,
                    'url_show'=> route('banner.show', $model->id),
                    'url_setposition'=> route('banner.setposition', $model->id),
                    'url_edit'=> route('banner.edit', $model->id),
                    'url_destroy'=> route('banner.destroy', $model->id)
                ]);
            })
            ->editColumn('created_at', function($model){
                return date('d-m-Y H:i:s', strtotime($model->created_at));
            })
            ->editColumn('created_by', function($model){
                $uti = new utility();
                return $uti->getUser($model->created_by);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'active', 'create_at', 'created_by'])
            ->make(true);
    }

    public function positionDataTable($id)
    {
        $model = DB::table('mst_banner_detail as a')
                ->join('mst_position as b','a.position_id','=','b.id')
                ->join('mst_page as c','b.page_id','=','c.id')
                ->where('a.banner_id',$id)
                ->select('c.name as page','b.name as position','a.*')
                ->get();

        return DataTables::of($model)
            ->addColumn('action', function($model){
                return view('master.banner.action-position', [
                    'model' => $model,
                    'url_destroy' => route('banner.deletepos', ['b'=>$model->banner_id,'p'=>$model->position_id])
                    // 'url_destroy' => route('banner.delete', $model->position_id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action','page'])
            ->make(true);
    }

    public function fetch(Request $request)
    {
        $value  = $request->get('value');

        $data = DB::table('mst_position')
                ->where('page_id','=',$value)
                ->where('is_deleted','0')
                ->where('active','1')
                ->get();
        
        $return = "<option value=''>- Select -</option>";
        foreach($data as $row)
        {
            $return .= "<option value='".$row->id."'>".$row->name."</option>";
        }
        echo $return;
    }
}
