<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstPosition;
use App\Models\MstPage;
use Illuminate\Support\Facades\Auth;
use Validator;
use DataTables;
use DB;
use App\Utility;

class MstPositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('master.position.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new MstPosition();
        $page = MstPage::where('active','1')
                    ->where('is_deleted','0')
                    ->pluck('name', 'id')
                    ->all();

        return view('master.position.form', compact(['model','page']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required|string',
            'page_id'=> 'required'
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;

        $data = [
            'name'          => $request->name,
            'page_id'       => $request->page_id,
            'active'        => $active,
            'created_by'    => $userId,
            'updated_by'    => $userId
        ];

        $model = MstPosition::create($data);
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $model = MstPosition::findOrFail($id);
        $model = DB::table('mst_position as a')
                    ->join('mst_page as b','a.page_id','=','b.id')
                    ->select('a.*','b.name as page')
                    ->first();
        $uti = new Utility();
        return view('master.position.detail', compact(['model','uti']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = MstPosition::findOrFail($id);
        $page = MstPage::where('active','1')
                    ->where('is_deleted','0')
                    ->pluck('name', 'id')
                    ->all();
        return view('master.position.form', compact(['model','page']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  => 'required|string',
            'page_id'=> 'required'
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;

        $data = [
            'name'          => $request->name,
            'page_id'       => $request->page_id,
            'active'        => $active,
            'updated_by'    => $userId
        ];

        $model = MstPosition::findOrFail($id);
        $model->update($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted_date   = date('Y-m-d H:i:s');
        $deleted_by     = Auth::user()->id;

        DB::update("update mst_position set 
            deleted_at='".$deleted_date."',
            deleted_by = ".$deleted_by.",
            is_deleted = 1
            where id = ".$id."");
    }

    public function dataTable()
    {
        $model = MstPosition::query();
        $model->where('is_deleted','<>','1');

        return DataTables::of($model)
            // ->addColumn('checkbox', '<input type="checkbox" id="'.$model->sec_role_id.'" name="checkbox">' )
            ->addColumn('action', function($model){
                return view('master.position.action', [
                    'model' => $model,
                    'url_show'=> route('position.show', $model->id),
                    'url_edit'=> route('position.edit', $model->id),
                    'url_destroy'=> route('position.destroy', $model->id)
                ]);
            })
            ->editColumn('created_at', function($model){
                return date('d-m-Y H:i:s', strtotime($model->created_at));
            })
            ->editColumn('created_by', function($model){
                $uti = new utility();
                return $uti->getUser($model->created_by);
            })
            ->editColumn('page_id', function($model){
                $page = MstPage::where('id', $model->page_id)->first();
                return $page->name;
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'page_id', 'active', 'create_at', 'created_by'])
            ->make(true);
    }
}
