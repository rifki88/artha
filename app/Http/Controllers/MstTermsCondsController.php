<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstTermConds;
use App\Models\MstTermsConds;
use Illuminate\Support\Facades\Auth;
use DataTables;
use DB;
use App\Utility;

class MstTermsCondsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.terms_conds.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new MstTermConds();
        // return view('master.terms_conds.form', compact('model'));
        return view('master.terms_conds.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'     => 'required|string',
            'desc'      => 'required|string'
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;

        $data = [
            'title'     => $request->title,
            'desc'      => $request->desc,
            'active'    => $active,
            'created_by'  => $userId,
            'updated_by'  => $userId
        ];

        $model = MstTermConds::create($data);
        if($model)
            return redirect('master/terms-conds/'.$model->id)->with('success', 'Success');
        else
            return redirect('master/terms-conds/create')->with('error', 'Failed');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = MstTermConds::findOrFail($id);
        $uti = new Utility();
        return view('master.terms_conds.detail', compact(['model','uti']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = MstTermConds::findOrFail($id);
        $uti = new Utility();
        return view('master.terms_conds.update', compact(['model','uti'])); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'     => 'required|string',
            'desc'      => 'required|string'
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;

        $data = [
            'title'     => $request->title,
            'desc'      => $request->desc,
            'active'    => $active,
            'updated_by'  => $userId
        ];

        $model = MstTermConds::findOrFail($id);
        $model->update($data);
        if($model)
            return redirect('master/terms-conds')->with('success', 'Success');
        else
            Redirect::back()->withErrors(['error', 'Failed']);
            // return redirect('master/help')->with('error', 'Failed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted_date   = date('Y-m-d H:i:s');
        $deleted_by     = Auth::user()->id;

        DB::update("update mst_terms_conds set 
            deleted_date='".$deleted_date."',
            deleted_by = ".$deleted_by.",
            is_deleted = 1
            where id = ".$id."");
    }

    public function delete($id)
    {
        $deleted_date   = date('Y-m-d H:i:s');
        $deleted_by     = Auth::user()->id;

        $delete = DB::update("update mst_terms_conds set 
            deleted_date='".$deleted_date."',
            deleted_by = ".$deleted_by.",
            is_deleted = 1
            where id = ".$id."");
        
        if($delete)
            return redirect('master/terms-conds')->with('success', 'Deleted');
        else
            Redirect::back()->withErrors(['error', 'Failed']);
            // return redirect('master/terms-conds')->with('error', 'Failed');
    }

    public function dataTable()
    {
        $model = MstTermConds::query();
        $model->where('is_deleted','<>','1');

        return DataTables::of($model)
            // ->addColumn('checkbox', '<input type="checkbox" id="'.$model->sec_role_id.'" name="checkbox">' )
            ->addColumn('action', function($model){
                return view('master.terms_conds.action', [
                    'model' => $model,
                    'url_show'=> route('terms-conds.show', $model->id),
                    'url_edit'=> route('terms-conds.edit', $model->id),
                    'url_destroy'=> route('terms-conds.destroy', $model->id)
                ]);
            })
            ->editColumn('created_at', function($model){
                return date('d-m-Y H:i:s', strtotime($model->created_at));
            })
            ->editColumn('created_by', function($model){
                $uti = new utility();
                return $uti->getUser($model->created_by);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'active', 'create_at', 'created_by'])
            ->make(true);
    }
}
