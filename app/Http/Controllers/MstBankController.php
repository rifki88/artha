<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstBank;
use Illuminate\Support\Facades\Auth;
use DataTables;
use DB;
use App\Utility;

class MstBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('master.bank.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new MstBank();
        return view('master.bank.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'          => 'required|string',
            'card_type'     => 'required|string',
            'image_logo'    => 'required'
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;


        $image = $request->file('image_logo');
        $filename = time().$image->getClientOriginalName();
        $path = base_path().'/public/images/bank/';
        $image->move($path, $filename);

        $data = [
            'name'      => $request->name,
            'card_type' => $request->card_type,
            'image_logo'=> $filename,
            'active'    => $active,
            'created_by'=> $userId,
            'updated_by'=> $userId
        ];

        $model = MstBank::create($data);
        return redirect('master/bank');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = MstBank::findOrFail($id);
        $uti = new Utility();
        return view('master.bank.detail', compact(['model','uti']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = MstBank::findOrFail($id);
        return view('master.bank.update', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'          => 'required|string',
            'card_type'     => 'required|string',
        ]);

        $model = MstBank::findOrFail($id);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;        

        if($request->file('image_logo'))
        {
            $image = $request->file('image_logo');
            $filename = time().$image->getClientOriginalName();
            $path = base_path().'/public/images/bank/';
            $image->move($path, $filename);

            $data = [
                'name'      => $request->name,
                'card_type' => $request->card_type,
                'image_logo'=> $filename,
                'active'    => $active,
                'updated_by'=> $userId
            ];

            if($model->image_logo != null)
                unlink(base_path().'/public/images/bank/'.$model->image_logo);            
        }
        else
        {
            $data = [
                'name'      => $request->name,
                'card_type' => $request->card_type,
                'active'    => $active,
                'updated_by'=> $userId
            ];   
        }

        $model->update($data);
        return redirect('master/bank');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $model = MstBank::findOrFail($id);
        // unlink(base_path().'/public/images/bank/'.$model->image_logo);
        // $model->delete();

        $deleted_date   = date('Y-m-d H:i:s');
        $deleted_by     = Auth::user()->id;

        DB::update("update mst_bank set 
            deleted_date='".$deleted_date."',
            deleted_by = ".$deleted_by.",
            is_deleted = 1
            where id = ".$id."");
    }

    public function dataTable()
    {
        $model = MstBank::query();
        $model->where('is_deleted','<>','1');

        return DataTables::of($model)
            // ->addColumn('checkbox', '<input type="checkbox" id="'.$model->sec_role_id.'" name="checkbox">' )
            ->addColumn('action', function($model){
                return view('master.bank.action', [
                    'model' => $model,
                    'url_show'=> route('bank.show', $model->id),
                    'url_edit'=> route('bank.edit', $model->id),
                    'url_destroy'=> route('bank.destroy', $model->id)
                ]);
            })
            ->editColumn('created_at', function($model){
                return date('d-m-Y H:i:s', strtotime($model->created_at));
            })
            ->addColumn('card_type', function($model){
                return $model->getCardType();
            })
            ->addColumn('image_logo', function($model){
                return "<img src='".'/images/bank/'.$model->image_logo."' width='100px' />";
            })
            ->editColumn('created_by', function($model){
                $uti = new utility();
                return $uti->getUser($model->created_by);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'card_type', 'image_logo', 'create_at', 'created_by'])
            ->make(true);
    }
}
