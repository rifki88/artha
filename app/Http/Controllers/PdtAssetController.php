<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PdtAsset;
use App\Models\PdtCategoryAsset;
use App\Models\MstTermConds;
use Illuminate\Support\Facades\Auth;
use Hash;
use DataTables;
use DB;
use App\Utility;

class PdtAssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.asset.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new PdtAsset();

        $category = PdtCategoryAsset::where('active','1')
                    ->where('is_deleted','0')
                    ->pluck('desc', 'id')
                    ->all();

        $termsconds = MstTermConds::where('active','1')
                    ->where('is_deleted','0')
                    ->pluck('title', 'id')
                    ->all();

        return view('master.asset.create', compact(['model','category','termsconds']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'asset_name' => 'required|string',
            'category_asset_id' => 'required',
            'desc' => 'required|string',
            'price_njop' => 'required',
            'price_market' => 'required',
            'credit_tenor' => 'required|string',
            'invesment_tenor' => 'required|string',
            'terms_conds_id' => 'required',
            'file_resume' => 'required',
            'images' => 'required',
            'owner_name' => 'required|string',
            'owner_ktp_number' => 'required|string',
            'owner_kk_number' => 'required|string',
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dataTable()
    {
        $model = PdtAsset::query();
        $model->where('is_deleted','<>','1');

        // $model = DB::table('pdt_asset as a')
        //         ->join('pdt_category_asset as b','a.category_asset_id','=','b.id')
        //         ->where('a.is_deleted','0')
        //         ->select('a.id as id','a.asset_name as asset_name','b.name as category','a.owner_name as owner_name','a.created_by as created_by','a.created_at as created_at')
        //         ->get();

        return DataTables::of($model)
            // ->addColumn('checkbox', '<input type="checkbox" id="'.$model->sec_role_id.'" name="checkbox">' )
            ->addColumn('action', function($model){
                return view('master.asset.action', [
                    'model' => $model,
                    'url_show'=> route('asset.show', $model->id),
                    'url_edit'=> route('asset.edit', $model->id),
                    'url_destroy'=> route('asset.destroy', $model->id)
                ]);
            })
            ->editColumn('created_at', function($model){
                return date('d-m-Y H:i:s', strtotime($model->created_at));
            })
            ->editColumn('created_by', function($model){
                $uti = new utility();
                return $uti->getUser($model->created_by);
            })
            ->editColumn('category', function($model){
                $category = PdtCategoryAsset::findOrFail($model->category_asset_id);
                return $category->name;
            })
            // ->addColumn('card_type', function($model){
            //     return $model->getCardType();
            // })
            // ->addColumn('image_logo', function($model){
            //     return "<img src='".'/images/bank/'.$model->image_logo."' width='100px' />";
            // })
            ->addIndexColumn()
            ->rawColumns(['action', 'category', 'create_at', 'created_by'])
            ->make(true);
    }
}
