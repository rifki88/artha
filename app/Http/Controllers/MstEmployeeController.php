<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstEmployee;
use Illuminate\Support\Facades\Auth;
use Hash;
use DataTables;
use DB;
use App\Utility;

class MstEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.employee.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new MstEmployee();
        $countries = DB::table('mst_countries')->get();
        return view('master.employee.create', compact(['model','countries']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'full_name'     => 'required|string',
            'username'      => 'required|string|unique:mst_employee,username,1,is_deleted',
            'password'      => 'required|required_with:password_confirmation|string|confirmed',
            'gender'        => 'required',
            'birth_place'   => 'required|string',
            'birth_date'    => 'required',
            'country'       => 'required',
            'province'       => 'required',
            'regency'       => 'required',
            'district'       => 'required',
            'village_id'       => 'required',
            'zip_code'       => 'required',
            'address'       => 'required',
            'email'       => 'required|string',
            'phone'       => 'required|string',
            'photo'       => 'required',
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;

        $image = $request->file('photo');
        $filename = time().$image->getClientOriginalName();
        $path = base_path().'/public/images/employee/';
        $image->move($path, $filename);

        $data = [
            'full_name'  => $request->full_name,
            'username'  => $request->username,
            'password'  => Hash::make($request->password),
            'gender'  => $request->gender,
            'birth_place'  => $request->birth_place,
            'birth_date'  => date('Y-m-d', strtotime($request->birth_date)),
            'village_id'  => $request->village_id,
            'address'  => $request->address,
            'zip_code'  => $request->zip_code,
            'email'  => $request->email,
            'phone'  => $request->phone,
            'photo'     => $filename,
            'active'    => $active,
            'created_by'=> $userId,
            'updated_by'=> $userId
        ];

        $model = MstEmployee::create($data);
        return redirect('master/employee/'.$model->id)->with('success','Employee successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = MstEmployee::findOrFail($id);
        $uti = new Utility();
        $address = $uti->getDetailAddress($model->village_id);
        return view('master.employee.detail', compact(['model', 'uti', 'address']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = MstEmployee::findOrFail($id);
        $uti = new Utility();
        $address = $uti->getDetailAddress($model->village_id);
        $countries = DB::table('mst_countries')->get();
        $provinces = DB::table('mst_provinces')->where('country_id',$address['country_id'])->get();
        $regencies = DB::table('mst_regencies')->where('provinces_id',$address['province_id'])->get();
        $districts = DB::table('mst_districts')->where('regency_id',$address['regency_id'])->get();
        $villages = DB::table('mst_villages')->where('district_id',$address['district_id'])->get();
        $model->birth_date = date('d-m-Y', strtotime($model->birth_date));

        return view('master.employee.update', 
                compact(['model', 
                        'countries', 
                        'provinces', 
                        'regencies', 
                        'districts', 
                        'villages', 
                        'uti', 
                        'address']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'full_name'     => 'required|string',
            'username'      => 'required|string|unique:mst_employee,username,'.$id.',id,is_deleted,0',
            'password'      => 'nullable|required_with:password_confirmation|string|confirmed',
            'gender'        => 'required',
            'birth_place'   => 'required|string',
            'birth_date'    => 'required',
            'country'       => 'required',
            'province'       => 'required',
            'regency'       => 'required',
            'district'       => 'required',
            'village_id'       => 'required',
            'zip_code'       => 'required',
            'address'       => 'required',
            'email'       => 'required|string',
            'phone'       => 'required|string',
        ]);

        $userId = Auth::user()->id;
        $active = $request->get('active') ? 1 : 0;

        $model = MstEmployee::findOrFail($id);

        if($request->file('photo'))
        {
            $image = $request->file('photo');
            $filename = time().$image->getClientOriginalName();
            $path = base_path().'/public/images/employee/';
            $image->move($path, $filename);

            if($request->password != '')
            {
                $data = [
                    'full_name'  => $request->full_name,
                    'username'  => $request->username,
                    'password'  => Hash::make($request->password),
                    'gender'  => $request->gender,
                    'birth_place'  => $request->birth_place,
                    'birth_date'  => date('Y-m-d', strtotime($request->birth_date)),
                    'village_id'  => $request->village_id,
                    'address'  => $request->address,
                    'zip_code'  => $request->zip_code,
                    'email'  => $request->email,
                    'phone'  => $request->phone,
                    'photo'     => $filename,
                    'active'    => $active,
                    'updated_by'=> $userId
                ];
            } else {
                $data = [
                    'full_name'  => $request->full_name,
                    'username'  => $request->username,
                    'gender'  => $request->gender,
                    'birth_place'  => $request->birth_place,
                    'birth_date'  => date('Y-m-d', strtotime($request->birth_date)),
                    'village_id'  => $request->village_id,
                    'address'  => $request->address,
                    'zip_code'  => $request->zip_code,
                    'email'  => $request->email,
                    'phone'  => $request->phone,
                    'photo'     => $filename,
                    'active'    => $active,
                    'updated_by'=> $userId
                ];
            }

            if($model->photo != null)
                unlink(base_path().'/public/images/employee/'.$model->photo);
        } else {
            if($request->password != '')
            {
                $data = [
                    'full_name'  => $request->full_name,
                    'username'  => $request->username,
                    'password'  => Hash::make($request->password),
                    'gender'  => $request->gender,
                    'birth_place'  => $request->birth_place,
                    'birth_date'  => date('Y-m-d', strtotime($request->birth_date)),
                    'village_id'  => $request->village_id,
                    'address'  => $request->address,
                    'zip_code'  => $request->zip_code,
                    'email'  => $request->email,
                    'phone'  => $request->phone,
                    'active'    => $active,
                    'updated_by'=> $userId
                ];
            } else {
                $data = [
                    'full_name'  => $request->full_name,
                    'username'  => $request->username,
                    'gender'  => $request->gender,
                    'birth_place'  => $request->birth_place,
                    'birth_date'  => date('Y-m-d', strtotime($request->birth_date)),
                    'village_id'  => $request->village_id,
                    'address'  => $request->address,
                    'zip_code'  => $request->zip_code,
                    'email'  => $request->email,
                    'phone'  => $request->phone,
                    'active'    => $active,
                    'updated_by'=> $userId
                ];
            }
        }

        if($model->update($data))
            return redirect('master/employee/'. $model->id)->with('success','Data Updated');
        else
            return redirect('master/employee/'. $model->id)->with('error','Failed to update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted_date   = date('Y-m-d H:i:s');
        $deleted_by     = Auth::user()->id;

        DB::update("update mst_employee set 
            deleted_date='".$deleted_date."',
            deleted_by = ".$deleted_by.",
            is_deleted = 1
            where id = ".$id."");
    }

    public function delete($id)
    {
        $deleted_date   = date('Y-m-d H:i:s');
        $deleted_by     = Auth::user()->id;

        $delete = DB::update("update mst_employee set 
            deleted_date='".$deleted_date."',
            deleted_by = ".$deleted_by.",
            is_deleted = 1
            where id = ".$id."");
        
        if($delete)
            return redirect('master/employee')->with('success', 'Deleted');
        else
            return redirect('master/employee')->with('error', 'Failed');
    }

    public function dataTable()
    {
        $model = MstEmployee::query();
        $model->where('is_deleted','<>','1');

        return DataTables::of($model)
            // ->addColumn('checkbox', '<input type="checkbox" id="'.$model->sec_role_id.'" name="checkbox">' )
            ->addColumn('action', function($model){
                return view('master.employee.action', [
                    'model' => $model,
                    'url_show'=> route('employee.show', $model->id),
                    'url_edit'=> route('employee.edit', $model->id),
                    'url_destroy'=> route('employee.destroy', $model->id)
                ]);
            })
            ->editColumn('created_at', function($model){
                return date('d-m-Y H:i:s', strtotime($model->created_at));
            })
            ->editColumn('created_by', function($model){
                $uti = new utility();
                return $uti->getUser($model->created_by);
            })
            // ->addColumn('card_type', function($model){
            //     return $model->getCardType();
            // })
            // ->addColumn('image_logo', function($model){
            //     return "<img src='".'/images/bank/'.$model->image_logo."' width='100px' />";
            // })
            ->addIndexColumn()
            ->rawColumns(['action', 'create_at', 'created_by'])
            ->make(true);
    }

    public function fetch(Request $request)
    {
        $select = $request->get('id');
        $value  = $request->get('value');
        $table = $request->get('table');
        $key = $request->get('key');

        $data = DB::table($table)
                ->where($key,'=',$value)
                ->get();
        
        $return = "<option value=''>- Select -</option>";
        foreach($data as $row)
        {
            $return .= "<option value='".$row->id."'>".$row->name."</option>";
        }
        echo $return;
    }
}
