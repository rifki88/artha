@php
    $method = $model->exists ? 'PUT' : 'POST';
@endphp
{!! Form::model($model, [
    'route' => $model->exists ? ['member.update', $model->id] : 'member.store',
    'method'=> $method,
]) !!}

    <div class="form-group">
        <label for="member" class="control-label">Nama Member</label>
        {!! Form::text('member', null, ['class'=>'form-control', 'id'=>'member']) !!}
    </div>

    <div class="form-group">
        <label for="active" class="control-label">Active</label>
        <div>
            {!! Form::checkbox('active', null, null, ['id'=>'active']) !!} 
        </div>
    </div>

{!! Form::close() !!}