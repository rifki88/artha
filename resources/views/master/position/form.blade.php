@php
    $method = $model->exists ? 'PUT' : 'POST';
@endphp
{!! Form::model($model, [
    'route' => $model->exists ? ['position.update', $model->id] : 'position.store',
    'method'=> $method,
]) !!}

    <div class="form-group">
        <label for="name" class="control-label">Name</label>
        {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name']) !!}
    </div>

    <div class="form-group">
        <label for="page_id" class="control-label">Page</label>
        {!! Form::select('page_id', [''=>'- Select -']+$page, null, ['class'=>'form-control', 'id'=>'page_id']) !!}
    </div>

    <div class="form-group">
        <label for="active" class="control-label">Active</label>
        <div>
            {!! Form::checkbox('active', null, null, ['id'=>'active']) !!} 
        </div>
    </div>

{!! Form::close() !!}