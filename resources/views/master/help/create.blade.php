@extends('base.main')
@section('title') Help @endsection
@section('page_icon') <i class="fa fa-user"></i> @endsection
@section('page_title') Create Help @endsection
@section('page_subtitle') create @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="{{ route('help.index') }}" class="btn btn-success" title="Manage">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
    {!! Form::model($model, [
        'route' => 'help.store',
        'method'=> 'post',
    ]) !!}
    <div class="box-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{  $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="title" class="control-label">Title*</label>
                    {!! Form::text('title', null, ['class'=>'form-control', 'id'=>'title']) !!}

                    @if($errors->has('title'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('title') }}
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="sub_title" class="control-label">Subtitle*</label>
                    {!! Form::text('sub_title', null, ['class'=>'form-control', 'id'=>'sub_title']) !!}

                    @if($errors->has('subtitle'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('subtitle') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="iframe" class="control-label">iFrame</label>
                    {!! Form::textarea('iframe', null, ['class'=>'form-control', 'id'=>'iframe', 'rows'=>2]) !!}

                    @if($errors->has('desc'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('desc') }}
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="active" class="control-label">Active</label>
                    <div>
                        {!! Form::checkbox('active', null, null, ['id'=>'active']) !!} 
                    </div>

                    @if($errors->has('active'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('active') }}
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="desc" class="control-label">Deskripsi*</label>
            {!! Form::textarea('desc', null, ['class'=>'textarea form-control', 'id'=>'desc', 'rows'=>6]) !!}

            @if($errors->has('desc'))
                <span class="invalid-feedback" role="alert">
                    {{ $errors->first('desc') }}
                </span>
            @endif
        </div>
    </div>
    <div class="box-footer">
        {!! Form::submit('Create', ['class'=>'btn btn-primary pull-right']) !!}
    </div>
    {!! Form::close() !!}
</div>
@endsection


