@extends('base.main')
@section('title') Help @endsection
@section('page_icon') <i class="fa fa-user"></i> @endsection
@section('page_title') Detail {{ $model->title }} @endsection
@section('page_subtitle') detail @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
        	<a href="{{ route('help.edit', $model->id) }}" class="btn btn-success" title="Edit Help">
                <i class="fa fa-edit"></i> Update
            </a>
             {{-- onclick="return confirm('Anda yakin?')" --}}
            <a href="{{ route('help.delete', $model->id) }}" class="btn btn-danger btn-delete2" title="Delete">
                <i class="fa fa-trash"></i> Delete
            </a>
        	<a href="{{ route('help.create') }}" class="btn btn-success" title="Create Help">
                <i class="fa fa-plus"></i> Create
            </a>
            <a href="{{ route('help.index') }}" class="btn btn-success" title="Manage">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
	<div class="box-body">
		<div class="row">
			<div class="col-md-7">
				<table class="table table-condensed table-striped">
					<tr>
						<th>Title</th>
						<td>{{ $model->title }}</td>
					</tr>
					<tr>
						<th>Subtitle</th>
						<td>{{ $model->sub_title }}</td>
					</tr>
					<tr>
						<th>Description</th>
						<td>{!! html_entity_decode($model->desc) !!}</td>
					</tr>
					<tr>
						<th>Active</th>
						<td>{{ $model->active == 1 ? 'Ya' : 'Tidak' }}</td>
					</tr>
					<tr>
						<th>Created By</th>
						<td>{{ $uti->getUser($model->created_by) }}</td>
					</tr>
					<tr>
						<th>Created Date</th>
						<td>{{ date('d-m-Y H:i:s', strtotime($model->created_at)) }}</td>
					</tr>
					<tr>
						<th>Updated By</th>
						<td>{{ $uti->getUser($model->updated_by) }}</td>
					</tr>
					<tr>
						<th>Updated Date</th>
						<td>{{ date('d-m-Y H:i:s', strtotime($model->updated_at)) }}</td>
					</tr>
				</table>
			</div>
			<div class="col-md-5">
				<div class="embed-responsive embed-responsive-16by9">
					{!! html_entity_decode($model->iframe) !!}
				</div>
			</div>
		</div>
	</div>
	<div class="box-body">
	</div>
</div>
@endsection