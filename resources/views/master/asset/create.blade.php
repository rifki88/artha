@extends('base.main')
@section('title') Asset @endsection
@section('page_icon') <i class="fa fa-cube"></i> @endsection
@section('page_title') Create Asset @endsection
@section('page_subtitle') list @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="{{ route('asset.index') }}" class="btn btn-success" title="Manage Asset">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
    {!! Form::model($model, [
        'route' => 'asset.store',
        'method'=> 'post',
        'enctype'   => 'multipart/form-data'
    ]) !!}
    <div class="box-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{  $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <h4>Asset</h4>
        <div class="row">
        	<div class="col-md-8">
        		<div class="form-group">
                    <label for="asset_name" class="control-label">Nama*</label>
                    {!! Form::text('asset_name', null, ['class'=>'form-control', 'id'=>'asset_name']) !!}

                    @if($errors->has('asset_name'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('asset_name') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
                <div class="form-group">
                    <label for="category_asset_id" class="control-label">Kategori*</label>
                    {!! Form::select('category_asset_id', [''=>'- Select -'] + $category, null, ['class'=>'form-control', 'id'=>'category_asset_id']) !!}

                    @if($errors->has('category_asset_id'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('category_asset_id') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
        		
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<div class="form-group">
                    <label for="desc" class="control-label">Deskripsi*</label>
                    {!! Form::textarea('desc', null, ['class'=>'form-control textarea', 'id'=>'desc', 'rows'=>6]) !!}

                    @if($errors->has('desc'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('desc') }}
                        </span>
                    @endif
                </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="price_njop" class="control-label">Harga NJOP*</label>
                    <div class="input-group">
                    	<span class="input-group-addon">
                    		Rp
                    	</span>
                    	{!! Form::text('price_njop', null, ['class'=>'form-control', 'id'=>'price_njop']) !!}
                    </div>

                    @if($errors->has('price_njop'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('price_njop') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="price_market" class="control-label">Harga Pasar*</label>
                    <div class="input-group">
                    	<span class="input-group-addon">
                    		Rp
                    	</span>
                    	{!! Form::text('price_market', null, ['class'=>'form-control', 'id'=>'price_market']) !!}
                    </div>

                    @if($errors->has('price_market'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('price_market') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="credit_tenor" class="control-label">Tenor Kredit*</label>
                    <div class="input-group">
                    	<span class="input-group-addon">
                    		Rp
                    	</span>
                    	{!! Form::text('credit_tenor', null, ['class'=>'form-control', 'id'=>'credit_tenor']) !!}
                    </div>

                    @if($errors->has('credit_tenor'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('credit_tenor') }}
                        </span>
                    @endif
                </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="invesment_tenor" class="control-label">Tenor Investasi*</label>
                    <div class="input-group">
                    	<span class="input-group-addon">
                    		Rp
                    	</span>
                    	{!! Form::text('invesment_tenor', null, ['class'=>'form-control', 'id'=>'invesment_tenor']) !!}
                    </div>

                    @if($errors->has('invesment_tenor'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('invesment_tenor') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="terms_conds_id" class="control-label">Term & Conditions*</label>
                    {!! Form::select('terms_conds_id', [''=>'- Select -'] + $termsconds, null, ['class'=>'form-control', 'id'=>'terms_conds_id']) !!}

                    @if($errors->has('terms_conds_id'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('terms_conds_id') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="file_resume" class="control-label">File Resume*</label>
                    {!! Form::file('file_resume', ['accept'=>'.pdf']) !!}

                    @if($errors->has('file_resume'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('file_resume') }}
                        </span>
                    @endif
                </div>
        	</div>
        </div>
        <div class="row">
        	{{-- <div class="col-md-6">
        		<label for="images" class="control-label">Atribut</label>
		        <div class="form-group">
		        </div>
        	</div> --}}
        	<div class="col-md-4">
		        <label for="images" class="control-label">Foto*</label>

		        <div class="field_wrapper">
			        <div class="form-group">
				        <input type="file" name="images[]" style="display: inline;" accept="image/x-png, image/jpeg">
		        		<a href="javascript:void(0);" class="add_button btn btn-xs btn-success pull-right" title="Add Field"><i class="fa fa-plus"></i></a>
			        </div>
		        </div>
		        @if($errors->has('images'))
                    <span class="invalid-feedback" role="alert">
                        {{ $errors->first('images') }}
                    </span>
                @endif
        	</div>
        </div>

        <hr>
        <h4>Owner</h4>
        <div class="row">
        	<div class="col-md-4">
                <div class="form-group">
                    <label for="owner_name" class="control-label">Nama Lengkap*</label>
                    {!! Form::text('owner_name', null, ['class'=>'form-control', 'id'=>'owner_name']) !!}

                    @if($errors->has('owner_name'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('owner_name') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
                <div class="form-group">
                    <label for="owner_ktp_number" class="control-label">Nomor KTP*</label>
                    {!! Form::text('owner_ktp_number', null, ['class'=>'form-control', 'id'=>'owner_ktp_number']) !!}

                    @if($errors->has('owner_ktp_number'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('owner_ktp_number') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="owner_kk_number" class="control-label">Nomor KK*</label>
                    {!! Form::text('owner_kk_number', null, ['class'=>'form-control', 'id'=>'owner_kk_number']) !!}

                    @if($errors->has('owner_kk_number'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('owner_kk_number') }}
                        </span>
                    @endif
                </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-4">
                <div class="form-group">
                    <label for="active" class="control-label">Active</label>
                    <div>
                        {!! Form::checkbox('active', null, null, ['id'=>'active']) !!} 
                    </div>

                    @if($errors->has('active'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('active') }}
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        {!! Form::submit('Create', ['class'=>'btn btn-primary pull-right']) !!}
    </div>
    {!! Form::close() !!}
</div>
@endsection

@push('scripts')
<script>
	var maxField = 10,
		addButton = $('.add_button'),
		wrapper = $('.field_wrapper'),
		fieldHtml = '<div class="form-group"><input type="file" name="images[]" style="display: inline;" accept="image/x-png, image/jpeg"><a href="javascript:void(0);" class="remove_button btn btn-xs btn-danger pull-right" title="Remove"><i class="fa fa-close"></i></a></div>',
		x = 1;

	$(addButton).click(function(){
		if(x < maxField){
			x++;
			$(wrapper).append(fieldHtml);
		}
	});

	$(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        // document.getElementsByClassName("form-group").remove();
        x--; //Decrement field counter
    });

</script>
@endpush