@extends('base.main')
@section('title') Class Price @endsection
@section('page_icon') <i class="fa fa-book"></i> @endsection
@section('page_title') Class Price @endsection
@section('page_subtitle') create @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="/master/class_price" class="btn btn-success"><i class="fa fa-list"></i> Manage</a>
        </div>
    </div>
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="box box-solid" >
	
		
		<div class="col-md-12" ><h3> Detail Class Price</h3> </div>
		<div class="col-md-6" style="background-color:#fff;">
			
            <div class="box-body">
                <div class="form-group">
                    <label for="created_by">Class<span class="required">*</span></label>
                    <p>{{$data[0]->name}}</p>
                </div>
				
				 <br>
				 <div class="form-group">
                    <label for="created_by">Created By<span class="required">*</span></label>
                    <p> {{$data[0]->username}} </p>
                </div>
					
				
            </div>
        
		</div>
		
		<div class="col-md-6" style="background-color:#fff;">
		
			<div class="box-body">
               
			    <div class="form-group">
                    <label for="price_start">Range Harga Rp <span class="required">*</span></label>
                   <p>Rp {{number_format($data[0]->price_start,0,',','.')}} s/d {{number_format($data[0]->price_start,0,',','.')}}</p>
                </div>

               <br><br>
				<div class="form-group">
                    <label for="price_start"> <span class="required"></span></label>
                   <p></p>
                </div>
				

				
            </div>
				
		</div>
    </div>
@endsection