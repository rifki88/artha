@extends('base.main')
@section('title') Class Price @endsection
@section('page_icon') <i class="fa fa-book"></i> @endsection
@section('page_title') Class Price @endsection
@section('page_subtitle') create @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="/master/class_price" class="btn btn-success"><i class="fa fa-list"></i> Manage</a>
        </div>
    </div>
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="box box-solid" >
	
		<div class="col-md-6" style="background-color:#fff;">
	
        <form action="/master/class_price/post_price" method="post" >
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="created_by">Class<span class="required">*</span></label>
                    <select class="form-control" name="class_id">
						<option value="">Choose Data</option>
						@foreach($data as $row)
							<option value="{{$row->id}}">{{$row->name}}</option>	
						@endforeach
					</select>
                </div>
				
				<br>
				<div class="box-footer" style="text-align:left;">
                    <input type="submit" value="Save" class="btn btn-primary"> 
                    <input type="button" value="Cancel" class="btn btn-primary" onclick="javascript:history.go(-1)"> 
                </div>

				
				
            </div>
        
		</div>
		
		<div class="col-md-6" style="background-color:#fff;">
		
			<div class="box-body">
               

                <div class="form-group">
                    <label for="price_end"> Range Harga Rp <span class="required">*</span></label><br>
                    <div class="col-md-6"><input type="text" name="price_end" id="price_end" class="form-control" placeholder="Price Start"> </div> 
					<div class="col-md-6"><input type="text" name="price_start" id="price_start" class="form-control" placeholder="Price End"></div>
                </div>
				
				
				
				
				
            </div>
				
			</form>		
				
		</div>
    </div>
@endsection