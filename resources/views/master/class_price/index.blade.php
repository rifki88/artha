@extends('base.main')
@section('title') Class Price @endsection
@section('page_icon') <i class="fa fa-user"></i> @endsection
@section('page_title') Class @endsection
@section('page_subtitle') list @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="/master/class_price/create" class="btn btn-success" title="Create Country">
                <i class="fa fa-plus"></i> Create
            </a>
        </div>
    </div>
@endsection

@section('content')
    <div class="box box-solid">
        <div class="box-header">
            <button class="btn btn-sm btn-danger btn-mass-delete"><i class="fa fa-trash"></i></button>
        </div>
        <div class="box-body">
            <table id="datatable" class="table table-hover table-condensed">
                <thead>
                    <tr>
                        {{-- <th></th> --}}
                        
                        <th>No</th>
                        <th>Class</th>
                        <th>Range Price</th>
                       
                        <th>Created By</th>
                        <th>Created Date</th>
                        <th>Action</th>
                        
                    </tr>
                </thead>
				<?php $no=1;?>
				@foreach($data as $datas)
				<tr>
					
					<td>{{$no}}</td>
					<td>{{$datas->name}}</td>
					<td> Rp {{number_format($datas->price_start,0,',','.') .' - '.number_format($datas->price_start,0,',','.')}}</td>
					
					<td>{{$datas->username}}</td>
					<td>{{$datas->created_at}}</td>
					<td>
						<a href="/master/class_price/show/{{$datas->id}}" class="btn-xs btn-primary"><i class="fa fa-search"></i></a>
						<a href="/master/class_price/edit/{{$datas->id}}" class="btn-xs btn-primary edit" ><i class="fa fa-edit"></i></a>
						<a href="/master/class_price/delete/{{$datas->id}}" class="btn-xs btn-danger"><i class="fa fa-trash"></i></a>
					</td>					
				</tr>
				<?php $no++;?>
				@endforeach
				
            </table>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        $('#datatable').DataTable({
            responsive : true,
            processing : true
           
        });
    </script>

@endpush


