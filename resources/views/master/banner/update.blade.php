@extends('base.main')
@section('title') Banner @endsection
@section('page_icon') <i class="fa fa-image"></i> @endsection
@section('page_title') Edit Banner @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="{{ route('banner.show', $model->id) }}" class="btn btn-success" title="Detail">
                <i class="fa fa-search"></i> Detail
            </a>
            <a href="{{ route('banner.setposition', $model->id) }}" class="btn btn-success" title="Set Position Banner">
                <i class="fa fa-map-marker"></i> Set Position
            </a>
            <a href="{{ route('banner.delete', $model->id) }}" class="btn btn-danger btn-delete2" title="Delete">
                <i class="fa fa-trash"></i> Delete
            </a>
            <a href="{{ route('banner.create') }}" class="btn btn-success" title="Create Banner">
                <i class="fa fa-plus"></i> Create
            </a>
            <a href="{{ route('banner.index') }}" class="btn btn-success" title="Manage">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
	{!! Form::model($model, [
	    'route' => ['banner.update', $model->id],
	    'method'=> 'put',
	    'enctype'	=> 'multipart/form-data'
	]) !!}
	<div class="box-body">
		@if(count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
					<li>{{  $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<div class="form-group">
		    <label for="title" class="control-label">Title*</label>
		    {!! Form::text('title', null, ['class'=>'form-control', 'id'=>'title']) !!}

		    @if($errors->has('title'))
		    	<span class="invalid-feedback" role="alert">
		    		{{ $errors->first('title') }}
		    	</span>
		    @endif
		</div>
		<div class="form-group">
		    <label for="subtitle" class="control-label">Subtitle*</label>
		    {!! Form::text('subtitle', null, ['class'=>'form-control', 'id'=>'subtitle']) !!}

		    @if($errors->has('subtitle'))
		    	<span class="invalid-feedback" role="alert">
		    		{{ $errors->first('subtitle') }}
		    	</span>
		    @endif
		</div>
		<div class="form-group">
            <label for="desc" class="control-label">Description*</label>
            {!! Form::textarea('desc', null, ['class'=>'textarea form-control', 'id'=>'desc', 'rows'=>6]) !!}

            @if($errors->has('desc'))
                <span class="invalid-feedback" role="alert">
                    {{ $errors->first('desc') }}
                </span>
            @endif
        </div>
        <div class="row">
        	<div class="col-md-6">
        		<div class="form-group">
        			@if($model->image == null)
						<img src="{{ asset('images/no-img.png') }}" class="img-responsive img-thumbnail">
					@else
						<img src="{{ asset('images/banner/'.$model->image) }}" class="img-responsive">
					@endif
        		</div>
        		<div class="form-group">
				    <label for="image" class="control-label">Image</label>
				    {!! Form::file('image', ['accept'=>'image/x-png, image/jpeg']) !!}

				    @if($errors->has('image'))
				    	<span class="invalid-feedback" role="alert">
				    		{{ $errors->first('image') }}
				    	</span>
				    @endif
				</div>
        	</div>
        	<div class="col-md-6">
        		<div class="form-group">
        			@if($model->video == null)
						<img src="{{ asset('images/no-img.png') }}" class="img-responsive img-thumbnail">
					@else
						<video width="320" height="240" controls>
							<source src="{{ asset('video/banner/'.$model->video) }}" type="video/mp4">
							<source src="{{ asset('video/banner/'.$model->video) }}" type="video/ogg">
							Your browser does not support the video
						</video>
					@endif
        		</div>
        		<div class="form-group">
				    <label for="video" class="control-label">Video</label>
				    {!! Form::file('video', ['accept'=>'video/*']) !!}

				    @if($errors->has('video'))
				    	<span class="invalid-feedback" role="alert">
				    		{{ $errors->first('video') }}
				    	</span>
				    @endif
				</div>
        	</div>
        </div>
        <div class="form-group">
			<label for="iframe" class="control-label">iFrame</label>
            {!! Form::textarea('iframe', null, ['class'=>'form-control', 'id'=>'iframe', 'rows'=>3]) !!}

            @if($errors->has('iframe'))
                <span class="invalid-feedback" role="alert">
                    {{ $errors->first('iframe') }}
                </span>
            @endif
        </div>
        <div class="form-group">
		    <label for="active" class="control-label">Active</label>
		    <div>
		        {!! Form::checkbox('active', null, null, ['id'=>'active']) !!} 
		    </div>
		    @if($errors->has('active'))
		    	<span class="invalid-feedback" role="alert">
		    		{{ $errors->first('active') }}
		    	</span>
		    @endif
		</div>
	</div>
	<div class="box-footer">
		{!! Form::submit('Save', ['class'=>'btn btn-primary pull-right']) !!}
	</div>
	{!! Form::close() !!}
</div>
@endsection