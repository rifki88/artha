@extends('base.main')
@section('title') Banner @endsection
@section('page_icon') <i class="fa fa-image"></i> @endsection
@section('page_title') Banner Detail @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
        	<a href="{{ route('banner.setposition', $model->id) }}" class="btn btn-success" title="Set Position Banner">
                <i class="fa fa-map-marker"></i> Set Position
            </a>
            <a href="{{ route('banner.edit', $model->id) }}" class="btn btn-success" title="Edit Banner">
                <i class="fa fa-edit"></i> Update
            </a>
            <a href="{{ route('banner.delete', $model->id) }}" class="btn btn-danger btn-delete2" title="Delete">
                <i class="fa fa-trash"></i> Delete
            </a>
        	<a href="{{ route('banner.create') }}" class="btn btn-success" title="Create Banner">
                <i class="fa fa-plus"></i> Create
            </a>
            <a href="{{ route('banner.index') }}" class="btn btn-success" title="Manage">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
	<div class="box-body">
		<h4>Detail</h4>
		<div class="row">
			<div class="col-md-6">
				<table class="table table-condensed table-striped">
					<tbody>
						<tr>
							<th>Title</th>
							<td>{!! $model->title !!}</td>
						</tr>
						<tr>
							<th>Subtitle</th>
							<td>{!! $model->subtitle !!}</td>
						</tr>
						<tr>
							<th>Description</th>
							<td>{!! html_entity_decode($model->desc) !!}</td>
						</tr>
						<tr>
							<th>Active</th>
							<td>{{ $model->active == 1 ? 'Ya' : 'Tidak' }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-6">
				<table class="table table-condensed table-striped">
					<tbody>
						<tr>
							<th>Created By</th>
							<td>{{ $uti->getUser($model->created_by) }}</td>
						</tr>
						<tr>
							<th>Created Date</th>
							<td>{{ date('d-m-Y H:i:s', strtotime($model->created_at)) }}</td>
						</tr>
						<tr>
							<th>Updated By</th>
							<td>{{ $uti->getUser($model->updated_by) }}</td>
						</tr>
						<tr>
							<th>Updated Date</th>
							<td>{{ date('d-m-Y H:i:s', strtotime($model->updated_at)) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7">
				<h4>Attachment</h4>
				<table class="table table-condensed table-striped">
					<tbody>
						<tr><th>Image</th></tr>
						<tr><td>
							@if($model->image == null)
								<img src="{{ asset('images/no-img.png') }}" class="img-responsive img-thumbnail">
							@else
								<img src="{{ asset('images/banner/'.$model->image) }}" class="img-responsive">
							@endif
						</td></tr>
						<tr><th>Video</th></tr>
						<tr><td>
							@if($model->video == null)
								<img src="{{ asset('images/no-img.png') }}" class="img-responsive img-thumbnail">
							@else
								<video width="440" height="300" controls>
									<source src="{{ asset('video/banner/'.$model->video) }}" type="video/mp4">
									<source src="{{ asset('video/banner/'.$model->video) }}" type="video/ogg">
									Your browser does not support the video
								</video>
							@endif
						</td></tr>
						<tr><th>iFrame</th></tr>
						<tr><td>{!! html_entity_decode($model->iframe) !!}</td></tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-5">
				<h4>Position</h4>
				<table class="table table-striped table-condensed">
					<thead>
						<tr>
							<th>Page</th>
							<th>Position</th>
							<th>Order</th>
						</tr>
					</thead>
					<tbody>
						@foreach($detail as $row)
							<tr>
								<td>{{ $row->page }}</td>
								<td>{{ $row->position }}</td>
								<td>{{ $row->order }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection