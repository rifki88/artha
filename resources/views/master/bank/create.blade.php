@extends('base.main')
@section('title') Bank @endsection
@section('page_icon') <i class="fa fa-bank"></i> @endsection
@section('page_title') Create Bank @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="{{ route('bank.index') }}" class="btn btn-success" title="Manage">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
	{!! Form::model($model, [
	    'route' => 'bank.store',
	    'method'=> 'post',
	    'enctype'	=> 'multipart/form-data'
	]) !!}
	<div class="box-body">
		@if(count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
					<li>{{  $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				    <label for="name" class="control-label">Nama Class</label>
				    {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name']) !!}

				    @if($errors->has('name'))
				    	<span class="invalid-feedback" role="alert">
				    		{{ $errors->first('name') }}
				    	</span>
				    @endif
				</div>
				<div class="form-group">
				    <label for="card_type" class="control-label">Tipe Kartu</label>
				    {!! Form::select('card_type', [''=>'- Select -'] + $model->cardTypeList(), null, ['class'=>'form-control','id'=>'card_type']) !!}

				    @if($errors->has('card_type'))
				    	<span class="invalid-feedback" role="alert">
				    		{{ $errors->first('card_type') }}
				    	</span>
				    @endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				    <label for="image_logo" class="control-label">Logo</label>
				    {!! Form::file('image_logo', ['accept'=>'image/x-png, image/jpeg', 'value'=>"{{ old('image_logo') }}"]) !!}

				    @if($errors->has('image_logo'))
				    	<span class="invalid-feedback" role="alert">
				    		{{ $errors->first('image_logo') }}
				    	</span>
				    @endif
				</div>
				<div class="form-group">
				    <label for="active" class="control-label">Active</label>
				    <div>
				        {!! Form::checkbox('active', null, null, ['id'=>'active']) !!} 
				    </div>
				    @if($errors->has('active'))
				    	<span class="invalid-feedback" role="alert">
				    		{{ $errors->first('active') }}
				    	</span>
				    @endif
				</div>
			</div>	
		</div>
	</div>
	<div class="box-footer">
		{!! Form::submit('Create', ['class'=>'btn btn-primary pull-right']) !!}
	</div>
	{!! Form::close() !!}
</div>
@endsection