@extends('base.main')
@section('title') Detail Terms & Conditions @endsection
@section('page_icon') <i class="fa fa-folder"></i> @endsection
@section('page_title') Detail Terms & Conditions @endsection
@section('page_subtitle') detail @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
        	<a href="{{ route('terms-conds.edit', $model->id) }}" class="btn btn-success" title="Edit Terms & Conds">
                <i class="fa fa-edit"></i> Update
            </a>
             {{-- onclick="return confirm('Anda yakin?')" --}}
            <a href="{{ route('terms-conds.delete', $model->id) }}" class="btn btn-danger btn-delete2" title="Delete">
                <i class="fa fa-trash"></i> Delete
            </a>
        	<a href="{{ route('terms-conds.create') }}" class="btn btn-success" title="Create Terms & Conds">
                <i class="fa fa-plus"></i> Create
            </a>
            <a href="{{ route('terms-conds.index') }}" class="btn btn-success" title="Manage">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<table class="table table-condensed table-striped">
					<tr>
						<th width="20%">Title</th>
						<td>{{ $model->title }}</td>
					</tr>
					<tr>
						<th>Description</th>
						<td>{!! html_entity_decode($model->desc) !!}</td>
					</tr>
					<tr>
						<th>Active</th>
						<td>{{ $model->active == 1 ? 'Ya' : 'Tidak' }}</td>
					</tr>
					<tr>
						<th>Created By</th>
						<td>{{ $uti->getUser($model->created_by) }}</td>
					</tr>
					<tr>
						<th>Created Date</th>
						<td>{{ date('d-m-Y H:i:s', strtotime($model->created_at)) }}</td>
					</tr>
					<tr>
						<th>Updated By</th>
						<td>{{ $uti->getUser($model->updated_by) }}</td>
					</tr>
					<tr>
						<th>Updated Date</th>
						<td>{{ date('d-m-Y H:i:s', strtotime($model->updated_at)) }}</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection