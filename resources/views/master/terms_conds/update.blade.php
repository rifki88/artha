@extends('base.main')
@section('title') Edit Terms & Conditions @endsection
@section('page_icon') <i class="fa fa-folder"></i> @endsection
@section('page_title') Edit Terms & Conditions @endsection
@section('page_subtitle') edit @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="{{ route('terms-conds.show', $model->id) }}" class="btn btn-success" title="Show">
                <i class="fa fa-search"></i> Show
            </a>
            <a href="{{ route('terms-conds.delete', $model->id) }}" class="btn btn-danger btn-delete2" title="Delete">
                <i class="fa fa-trash"></i> Delete
            </a>
        	<a href="{{ route('terms-conds.create') }}" class="btn btn-success" title="Create Help">
                <i class="fa fa-plus"></i> Create
            </a>
            <a href="{{ route('terms-conds.index') }}" class="btn btn-success" title="Manage">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
    {!! Form::model($model, [
        'route' => ['terms-conds.update', $model->id],
        'method'=> 'put',
    ]) !!}
    <div class="box-body">
		@if(count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
					<li>{{  $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				    <label for="title" class="control-label">Title*</label>
				    {!! Form::text('title', null, ['class'=>'form-control', 'id'=>'title']) !!}

				    @if($errors->has('title'))
				    	<span class="invalid-feedback" role="alert">
				    		{{ $errors->first('title') }}
				    	</span>
				    @endif
				</div>
				<div class="form-group">
				    <label for="desc" class="control-label">Deskripsi*</label>
		            {!! Form::textarea('desc', null, ['class'=>'textarea form-control', 'id'=>'desc', 'rows'=>6]) !!}

		            @if($errors->has('desc'))
		                <span class="invalid-feedback" role="alert">
		                    {{ $errors->first('desc') }}
		                </span>
		            @endif
				</div>
				<div class="form-group">
                    <label for="active" class="control-label">Active</label>
                    <div>
                        {!! Form::checkbox('active', null, null, ['id'=>'active']) !!} 
                    </div>

                    @if($errors->has('active'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('active') }}
                        </span>
                    @endif
                </div>
			</div>
		</div>
	</div>
    <div class="box-footer">
        {!! Form::submit('Save', ['class'=>'btn btn-primary pull-right']) !!}
    </div>
    {!! Form::close() !!}
</div>
@endsection