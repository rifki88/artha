@extends('base.main')
@section('title') Dashboard @endsection
@section('page_icon') <i class="fa fa-dashboard"></i> @endsection
@section('page_title') Dashboard @endsection
@section('page_subtitle') Monitor @endsection

@section('content')
    <div class="box box-solid">
        <div class="box-header">
            <h3 class="box-title">Monitor {{ Auth::user()->username }} {{ Auth::user()->sec_employee_id }}</h3>
            @php
                // var_dump( Auth::user() );
                // print_r(Auth::user());
            @endphp
        </div>
        <div class="box-body">
            test
        </div>
    </div>
@endsection