<!DOCTYPE html>
<html lang="en">
<head>
    @include('base.head')
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
    <div class="wrapper">
        
        @include('base.nav')
        
        @include('base.menu')

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    @yield('page_icon')
                    @yield('page_title')
                    <small>@yield('page_subtitle')</small>
                </h1>
                @yield('breadcrumb')
            </section>

            <section class="content">
                {{-- flash message --}}
                @if(session()->get('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ session()->get('success') }} 
                    </div>
                @elseif(session()->get('error'))
                    <div class="alert alert-error alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ session()->get('error') }}  
                    </div>
                @endif
                {{-- menu --}}
                @yield('menu')
                {{-- content --}}
                @yield('content')
            </section>
        </div>
        @include('base.modal')

        @include('base.footer')

        <div class="control-sidebar-bg"></div>
    </div>

    @include('base.script')
</body>
</html>