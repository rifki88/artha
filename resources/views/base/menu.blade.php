<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('images/employee/'.Auth::user()->photo) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->full_name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
{{-- Dashboard --}}
            <li class="treeview {{ Request::is('site') || Request::is('/') ? 'active' : null }}">
                <a href="{{ route('site.index') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is('site.index') ? 'active' : null }}">
                    	<a href="{{ route('site.index') }}">
                            <i class="fa fa-circle-o"></i> Aset Terlaris
                        </a>
                    </li>
                    <li class="{{ Request::is('site.index') ? 'active' : null }}">
                    	<a href="{{ route('site.index') }}">
                            <i class="fa fa-circle-o"></i> Investor Tertinggi
                        </a>
                    </li>
                </ul>
            </li>
{{-- End Dashboard --}}
{{-- Master --}}
            <li class="treeview {{ Request::is('master/*') ? 'active' : null }}">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Master</span>
                </a>
                <ul class="treeview-menu">
                   
                    <li class="{{ Request::is('master/member') ? 'active' : null }}">
                        <a href="{{ route('member.index') }}">
                            <i class="fa fa-circle-o"></i> Member
                        </a>
                    </li>
					
                    <li class="{{ Request::is('master/bank') || Request::is('master/bank/*') ? 'active' : null }}">
                        <a href="{{ route('bank.index') }}">
                            <i class="fa fa-circle-o"></i> Bank
                        </a>
                    </li>
					
					<li class="treeview {{ Request::is('master/class') || Request::is('master/class/*') || Request::is('master/class_price') || Request::is('master/class_price/*') ? 'active' : null }}">
						<a href="#">
		                    <i class="fa fa-folder"></i> <span>Class</span>
		                </a>
						<ul class="treeview-menu">    
		                    <li class="{{ Request::is('master/class') ? 'active' : null }}">
		                        <a href="{{ route('class.index') }}">
		                            <i class="fa fa-circle-o"></i> Data Class
		                        </a>
		                    </li>
							
							 <li class="{{ Request::is('master/class_price') ? 'active' : null }}">
		                        <a href="/master/class_price">
		                            <i class="fa fa-circle-o"></i> Price Class
		                        </a>
		                    </li>
						</ul>
					</li>
					
					<li class="treeview {{ Request::is('location/*') ? 'active' : null }}">
		                <a href="#">
		                    <i class="fa fa-folder"></i> <span>Address</span>
		                </a>
		                <ul class="treeview-menu">
		                    
							<!--li class="{{ Request::is('location/address') ? 'active' : null }}">
					                        <a href="/master/data_address">
					                            <i class="fa fa-circle-o"></i> Data Address
					                        </a>
					                    </li-->
										
							<li class="{{ Request::is('location/country') ? 'active' : null }}">
		                        <a href="/master/country">
		                            <i class="fa fa-circle-o"></i> Country
		                        </a>
		                    </li>
							
							<li class="{{ Request::is('location/province') ? 'active' : null }}">
		                        <a href="/master/province">
		                            <i class="fa fa-circle-o"></i> Province
		                        </a>
		                    </li>
							
							<li class="{{ Request::is('location/regency') ? 'active' : null }}">
		                        <a href="/master/regency">
		                            <i class="fa fa-circle-o"></i> Regency
		                        </a>
		                    </li>
							
							<li class="{{ Request::is('location/district') ? 'active' : null }}">
		                        <a href="/master/district">
		                            <i class="fa fa-circle-o"></i> District
		                        </a>
		                    </li>
							
							<li class="{{ Request::is('location/village') ? 'active' : null }}">
		                        <a href="/master/village">
		                            <i class="fa fa-circle-o"></i> Village
		                        </a>
		                    </li>
		                </ul>
		            </li>

		            <li class="{{ Request::is('master/employee') || Request::is('master/employee/*') ? 'active' : null }}">
						<a href="{{ route('employee.index') }}">
							<i class="fa fa-circle-o"></i> Employee
						</a>
					</li>

					<li class="{{ Request::is('master/position') ? 'active' : null }}">
						<a href="{{ route('position.index') }}">
							<i class="fa fa-circle-o"></i> Position
						</a>
					</li>
					
					<li class="{{ Request::is('master/page') ? 'active' : null }}">
						<a href="{{ route('page.index') }}">
							<i class="fa fa-circle-o"></i> Page
						</a>
					</li>
					
					<li class="{{ Request::is('master/banner') || Request::is('master/banner/*') ? 'active' : null }}">
						<a href="{{ route('banner.index') }}">
							<i class="fa fa-circle-o"></i> Banner
						</a>
					</li>
					
					<li class="{{ Request::is('master/terms-conds') || Request::is('master/terms-conds/*') ? 'active' : null }}">
						<a href="{{ route('terms-conds.index') }}">
							<i class="fa fa-circle-o"></i> Term and Condition
						</a>
					</li>
					
					<li class="treeview {{ Request::is('asset/*') ? 'active' : null }}">
						<a href="#">
		                    <i class="fa fa-folder"></i> <span>Asset</span>
		                </a>
						<ul class="treeview-menu">    
		                    <li class="{{ Request::is('master/asset-category') ? 'active' : null }}">
		                        <a href="{{ route('asset-category.index') }}">
		                            <i class="fa fa-circle-o"></i> Category 
		                        </a>
		                    </li>
							 <li class="{{ Request::is('master/asset') ? 'active' : null }}">
		                        <a href="#">
		                            <i class="fa fa-circle-o"></i>  Asset
		                        </a>
		                    </li>
						</ul>
					</li>

					<li class="treeview {{ Request::is('news/*') ? 'active' : null }}">
		                <a href="#">
		                    <i class="fa fa-folder"></i> <span>News</span>
		                </a>
		                <ul class="treeview-menu">
						
							<li class="{{ Request::is('asset/category') ? 'active' : null }}">
		                        <a href="#">
		                            <i class="fa fa-circle-o"></i> Category News
		                        </a>
		                    </li>
							
							
		                    <li class="{{ Request::is('asset/tag') ? 'active' : null }}">
		                        <a href="#">
		                            <i class="fa fa-circle-o"></i> Tag News
		                        </a>
		                    </li>
							
							<li class="{{ Request::is('asset/data') ? 'active' : null }}">
		                        <a href="#">
		                            <i class="fa fa-circle-o"></i> Data News
		                        </a>
		                    </li>
							
		                </ul>
		            </li>
					
                    <li class="{{ Request::is('master/help') ? 'active' : null }}">
                        <a href="{{ route('help.index') }}">
                            <i class="fa fa-circle-o"></i> Help
                        </a>
                    </li>

                    <li class="{{ Request::is('privacy_policy') ? 'active' : null }}">
						<a href="#">
							<i class="fa fa-circle-o"></i> Privacy Policy
						</a>
					</li>
					
					<li class="{{ Request::is('contact_us') ? 'active' : null }}">
						<a href="#">
							<i class="fa fa-circle-o"></i> Contact Us
						</a>
					</li>

                </ul>
            </li>
{{-- End Master --}}	
{{-- Transaction --}}
			<li class="treeview {{ Request::is('transaction/*') ? 'active' : null }}">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Transaction</span>
                </a>
                <ul class="treeview-menu">
				
					<li class="{{ Request::is('transcation/investor') ? 'active' : null }}">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> Investor Details
                        </a>
                    </li>					
				
                </ul>
            </li>
{{-- End Transaction --}}
{{-- Setting --}}
            <li class="treeview {{ Request::is('setting/*') ? 'active' : null }}">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Setting</span>
                </a>
                <ul class="treeview-menu">
				
					<li class="{{ Request::is('master/role') ? 'active' : null }}">
                        <a href="{{ route('role.index') }}">
                            <i class="fa fa-circle-o"></i> Role
                        </a>
                    </li>
					
                    <li class="{{ Request::is('setting/menu') ? 'active' : null }}">
                        <a href="{{ route('menu.index') }}">
                            <i class="fa fa-circle-o"></i> Menu
                        </a>
                    </li>
					
					<li class="{{ Request::is('setting/sec_accessrole') ? 'active' : null }}">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> Access Role
                        </a>
                    </li>
					
					<li class="{{ Request::is('setting/sec_accessuser') ? 'active' : null }}">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> Access User
                        </a>
                    </li>
					
                </ul>
            </li>
{{-- End Setting --}}		
        </ul>
    </section>
<!-- /.sidebar -->
</aside>