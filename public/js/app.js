$('body').on('click', '.modal-show', function(e){
    e.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title');
    
    $('#modal-title').text(title);
    $('#modal-btn-save').removeClass('hide').text(me.hasClass('edit') ? 'Update' : 'Create');
    
    $.ajax({
        url: url,
        dataType: 'html',
        success: function(response)
        {
            $('#modal-body').html(response);
        }
    });

    $('#modal').modal('show');
});

$('#modal-btn-save').click(function(e){
    e.preventDefault();

    var form = $('#modal-body form'),
        url = form.attr('action'),
        method = $('input[name=_method]').val() == undefined ? 'post' : 'put';

    form.find('.help-block').remove();
    form.find('.form-group').removeClass('has-error');
    
    $.ajax({
        url : url,
        method : method,
        data : form.serialize(),
        success : function(response){
            form.trigger('reset');
            $('#modal').modal('hide');
            $('#datatable').DataTable().ajax.reload();

            swal({
                type : 'success',
                title : 'Success',
                message : 'Data berhasil disimpan'
            });
        },
        error : function(e){
            var response = e.responseJSON;
            // console.log(response);
            if($.isEmptyObject(response) == false)
            {
                $.each(response.errors, function(key, value) {
                    $('#' + key)
                        .closest('.form-group')
                        .addClass('has-error')
                        .append('<span class="help-block">'+ value +'</span>')
                });
            }
        }
    });
});

$('body').on('click', '.btn-delete', function(e){
    e.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title'),
        csrf_token = $('meta[name="csrf-token"]').attr('content');
    
    swal({
        title : 'Anda yakin menghapus '+title+' ?',
        type : 'warning',
        showCancelButton : true,
        confirmButtonColor : '#3085d6',
        cancelButtonColor : '#d33',
        confirmButtonText : 'Ya, hapus!'
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url : url,
                type : 'post',
                data : {
                    '_method': 'DELETE',
                    '_token' : csrf_token
                },
                success : function(r){
                    $('#datatable').DataTable().ajax.reload();
                    swal({
                        type : 'success',
                        title : 'Success',
                        text : 'Data berhasil dihapus'
                    });
                },
                error : function(er){
                    swal({
                        type : 'error',
                        title : 'Failed',
                        text : 'Data gagal dihapus'
                    });
                }
            });
        }
    });

});

$('body').on('click', '.btn-delete2', function(e){
    e.preventDefault();

    var link = $(this).attr('href');

    swal({
        title : 'Anda yakin ?',
        type : 'warning',
        showCancelButton : true,
        confirmButtonColor : '#3085d6',
        cancelButtonColor : '#d33',
        confirmButtonText : 'Ya, hapus!'
    }).then((result)=>{
        if(result.value){
            window.location = link;
        }
    });

});

$('body').on('click', '.btn-delete3', function(e){
    e.preventDefault();

    var link = $(this).attr('href');

    swal({
        title : 'Anda yakin ?',
        type : 'warning',
        showCancelButton : true,
        confirmButtonColor : '#3085d6',
        cancelButtonColor : '#d33',
        confirmButtonText : 'Ya, hapus!'
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url : link,
                type : 'get',
                success : function(r){
                    $('#datatable').DataTable().ajax.reload();
                    swal({
                        type : 'success',
                        title : 'Success',
                        text : 'Data berhasil dihapus'
                    });
                },
                error : function(er){
                    swal({
                        type : 'error',
                        title : 'Failed',
                        text : 'Data gagal dihapus'
                    });
                }
            });
        }
    });

});

$('body').on('click', '.btn-show', function(e){
    e.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title'); 

    $('#modal-title').text(title);
    $('#modal-btn-save').addClass('hide');

    $.ajax({
        url :url,
        dataType : 'html',
        success : function(response){
            $('#modal-body').html(response);
        }
    });

    $('#modal').modal('show');
});