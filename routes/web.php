<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes();
Route::group(['middleware'=>['auth']], function(){

    // Route::get('signout', 'WebController@destroy');
    // Root
    Route::get('/', 'SiteController@index');
    // Site
    Route::resource('site', 'SiteController');

    // Master
    Route::group(['prefix'=>'master'], function(){
        // Role
        Route::resource('role', 'SecRoleController');
        Route::get('table/role', 'SecRoleController@dataTable')->name('table.role');
        // Member Type
        Route::resource('member', 'MstMemberController');
        Route::get('table/member', 'MstMemberController@dataTable')->name('table.member');
        // Class
        Route::resource('class', 'MstClassController');
        Route::get('table/class', 'MstClassController@dataTable')->name('table.class');
		// Bank
        Route::resource('bank', 'MstBankController');
        Route::get('table/bank', 'MstBankController@dataTable')->name('table.bank');
        // Asset Category
        Route::resource('asset-category', 'PdtCategoryAssetController');
        Route::get('table/asset-category', 'PdtCategoryAssetController@dataTable')->name('table.asset-category');
        // Help / FAQ
        Route::resource('help', 'MstHelpController');
        Route::get('table/help', 'MstHelpController@dataTable')->name('table.help');
        Route::get('help/delete/{id}', 'MstHelpController@delete')->name('help.delete');
        // Employee
        Route::resource('employee', 'MstEmployeeController');
        Route::get('table/employee', 'MstEmployeeController@dataTable')->name('table.employee');
        Route::post('employee/fetch', 'MstEmployeeController@fetch')->name('employee.fetch');
        Route::get('employee/delete/{id}', 'MstEmployeeController@delete')->name('employee.delete');
        // Page
        Route::resource('page', 'MstPageController');
        Route::get('table/page', 'MstPageController@dataTable')->name('table.page');
        // Terms & Conditions
        Route::resource('terms-conds', 'MstTermsCondsController');
        Route::get('table/terms-conds', 'MstTermsCondsController@dataTable')->name('table.terms-conds');
        Route::get('terms-conds/delete/{id}', 'MstTermsCondsController@delete')->name('terms-conds.delete');
        // Banner
        Route::resource('banner', 'MstBannerController');
        Route::get('table/banner', 'MstBannerController@dataTable')->name('table.banner');
        Route::get('banner/setposition/{id}','MstBannerController@setposition')->name('banner.setposition');
        Route::get('banner/delete/{id}', 'MstBannerController@delete')->name('banner.delete');
        Route::post('banner/set','MstBannerController@set')->name('banner.set');
        Route::post('banner/fetch', 'MstBannerController@fetch')->name('banner.fetch');
        Route::get('banner/deletepos/b/{b}/p/{p}', 'MstBannerController@deleteposition')->name('banner.deletepos');
        Route::get('table/banner/position/{id}', 'MstBannerController@positionDataTable')->name('table.banner.position');
        // Position
        Route::resource('position', 'MstPositionController');
        Route::get('table/position', 'MstPositionController@dataTable')->name('table.position');
        // Asset
        Route::resource('asset', 'PdtAssetController');
        Route::get('table/asset', 'PdtAssetController@dataTable')->name('table.asset');
    });
    // Setting
    Route::group(['prefix'=>'setting'], function(){
       // Module
        Route::resource('menu', 'SecModuleController');
        Route::get('table/menu', 'SecModuleController@dataTable')->name('table.menu'); 
    });
		
	Route::get('/master/class_price','ClassPriceController@index');
	Route::get('/master/class_price/create','ClassPriceController@create');
	Route::get('/master/class_price/delete/{id}','ClassPriceController@destroy');
	Route::get('/master/class_price/edit/{id}','ClassPriceController@edit');
	Route::post('/master/class_price/post_price/','ClassPriceController@store');
	Route::post('/master/class_price/edit_price/{id}','ClassPriceController@update');
	Route::get('/master/class_price/edit/{id}','ClassPriceController@edit');
	Route::get('/master/class_price/show/{id}','ClassPriceController@show');
	
	Route::get('/master/country','CountryController@index');
	Route::get('/master/country/create','CountryController@create');
	Route::post('/master/country/post_country','CountryController@store');
	Route::get('/master/country/edit/{id}','CountryController@edit');
	Route::post('/master/country/edit_country/{id}','CountryController@update');
	Route::get('/master/country/import','CountryController@import');
	Route::post('/master/country/proses_import','CountryController@proses_import');

	Route::get('/master/province','ProvinceController@index');
	Route::get('/master/province/create','ProvinceController@create');
	Route::post('/master/province/post_province','ProvinceController@store');
	Route::post('/master/province/edit_province/{id}','ProvinceController@update');
	Route::get('/master/province/delete/{id}','ProvinceController@destroy');
	Route::get('/master/province/edit/{id}','ProvinceController@edit');
	Route::get('/master/province/delete/{id}','ProvinceController@destroy');
	Route::get('/master/province/import','ProvinceController@import');
	Route::post('/master/province/import_province','ProvinceController@import_province');
	
	Route::get('/master/regency','RegencyController@index');
	Route::get('/master/regency/create','RegencyController@create');
	Route::post('/master/regency/post_regency','RegencyController@store');
	Route::get('/master/regency/edit_regency/{id}','RegencyController@edit');
	Route::post('/master/regency/regency_edit/{id}','RegencyController@update');
	Route::get('/master/regency/delete_regency/{id}','RegencyController@destroy');
	Route::post('/master/regency/regency_edit/{id}','RegencyController@update');
	Route::get('/master/regency/import','RegencyController@import');
	Route::post('/master/regency/import_post_regency','RegencyController@import_regency');
	
	Route::get('/master/district','DistrictController@index');
	Route::get('/master/district/create','DistrictController@create');
	Route::post('/master/district/post_district','DistrictController@store');
	Route::get('master/district/edit/{id}','DistrictController@edit');
	Route::post('/master/district/edit_district/{id}','DistrictController@update');
	Route::get('/master/district/delete_district/{id}','DistrictController@destroy');
	Route::get('/master/district/import','DistrictController@import');
	Route::post('/master/district/import_district','DistrictController@import_district');
	
	Route::get('/master/village','VillageController@index');
	Route::get('/master/village/create','VillageController@create');
	Route::post('/master/village/post_village','VillageController@store');
	Route::get('master/village/edit_village/{id}','VillageController@edit');
	Route::post('/master/village/edit_village_post/{id}','VillageController@update');
	Route::get('/master/village/delete_village/{id}','VillageController@destroy');
	Route::get('/master/village/import','VillageController@import');
	Route::post('/master/village/import_village','VillageController@import_village');
	
	
	Route::get('/master/data_address','MstAddressController@index');
	Route::get('/master/address/create','MstAddressController@create');
	Route::get('/master/address/province/{id}','MstAddressController@province');
	Route::get('/master/address/regency/{id}','MstAddressController@regency');
	Route::get('/master/address/district/{id}','MstAddressController@district');
	Route::get('/master/address/village/{id}','MstAddressController@village');
	Route::post('/master/address/post_address','MstAddressController@store');
});





// Route::get('/home', 'HomeController@index')->name('home');
